# Coding Challenge for Invision -- SimpySocial app

## Requirement

At a minimum, we’d like to see the following screens implemented:

- The index page
- The modal window to create a new post (accessed by clicking on the new chat icon in main nav)
- The modal window to create a new user (accessed by clicking on profile photo in main nav), tied into our email validation service API

You are free to build out other pages indicated in the prototype, but these are not required.
The end product should:

- Be built in HTML5, CSS, and JavaScript
- Use a CSS processor and front end build tool of your choice.
- Be compatible with latest versions of Chrome, Safari, Firefox, and IE
- Not rely on HTML/CSS frameworks; while their use is not prohibited, the markup should be as clean and free of unnecessary elements
- Be responsive
- Be as faithful to the design as possible

## Guidelines

- This is a front-end development demonstration only; no back-end or data persistence is required
- You are free to demonstrate any improvement on the design as you’d like, but we ask that you do so in a separate copy of the project.
- Also, tie the email address for "Create new user" to the BriteVerify email validation service. You can find documentation on their API here: http://docs.briteverify.com/real-time-api/
API Key: <redacted -- public repo>